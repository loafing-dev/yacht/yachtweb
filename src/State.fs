﻿module State

open ModernYachtCore.Model

let mutable roll : int = 0
let mutable place : int = -1

let mutable closed : bool = true

let mutable board : BoardRecord = DefaultBoard

let mutable player : PlayerRecord = 
    {
        name = "Default"
        scores = ModernYachtCore.Board.EmptyTable
    }

let mutable game : GameRecord =
    {
        players = seq { player }
        board = board
    }