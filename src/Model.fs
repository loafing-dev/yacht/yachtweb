﻿module Model

type DieRecord = 
    { 
        Image : Browser.Types.HTMLImageElement
        Checkbox: Browser.Types.HTMLInputElement
    }

type RowRecord = 
    {
        Title: Browser.Types.HTMLSpanElement
        Label: Browser.Types.HTMLLabelElement
        Span: Browser.Types.HTMLSpanElement
        Radio: Browser.Types.HTMLInputElement
    }

type MenuRowData = 
    {
        Title : string
        Click : (Browser.Types.MouseEvent -> unit)
    }