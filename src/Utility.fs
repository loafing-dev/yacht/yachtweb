﻿module Utility

open Model
open Bindings
open State
open Zanaptak.PcgRandom
open ModernYachtCore.Model
open Browser


let rand = Pcg()

let getplace _ =
    Seq.iteri
        (fun (index : int)(row : RowRecord) 
            ->
                if not (index = 14)
                then
                    if row.Radio.``checked`` = true
                    then
                        place <- index
                        printf "place: %i index: %i" place index
                        row.Radio.``checked`` <- false)
        rows

let getholds _ =
    board <- 
        {
            hold = 
                if not (roll = 0)
                then
                    Seq.ofList (List.map
                        (fun (record : DieRecord) 
                            -> record.Checkbox.``checked`` = true)
                        dice)
                else seq { false; false; false; false; false; }
            dice = board.dice
            roll = board.roll
            playerid = board.playerid
            playercount = board.playercount
        }
    game <- { board = board; players = game.players }

let rollDie (old : int)(keep : bool) =
    let newdie = (rand.Next 6) + 1
    if keep then old else newdie

let controlsupdate (roll : int) = 
    if roll = 3 
    then 
        rollbutton.classList.add "blockbutton"
        placebtn.classList.remove "blockbutton"
        List.iter 
            (fun (record : DieRecord) -> record.Checkbox.readOnly <- true)
            dice
    elif roll = 0
    then
        rollbutton.classList.remove "blockbutton"
        placebtn.classList.add "blockbutton"
        List.iter 
            (fun (record : DieRecord) -> record.Checkbox.readOnly <- true)
            dice
    else
        rollbutton.classList.remove "blockbutton"
        placebtn.classList.remove "blockbutton"
        List.iter 
            (fun (record : DieRecord) -> record.Checkbox.readOnly <- false)
            dice
    placeout.innerText <- if roll = 0 then "New roll!" else sprintf "Roll %i!" roll

let displayScores (input : int seq) = 
    Seq.iteri 
        (fun (index : int)(row : int) -> 
            rows.[index].Span.innerText <- if row = -1 then "_" else string row) 
        input
    let score = Seq.sumBy (fun (x : int) -> if x = -1 then 0 else x) input
    scoreout.innerText <- string score

let displayDie (dicelist : int seq) (keeps : bool seq): unit = 
    Seq.iteri2 
        (fun (index : int) (die : int) (keep : bool)
            ->
                (List.item index dice).Image.src <- ("c/c" + string die + ".svg")
                (List.item index dice).Checkbox.``checked`` <- keep
                (List.item index dice).Image.alt <- sprintf "dice #%i is a %i" (index + 1) die
                (List.item index dice).Image.title <- sprintf "dice #%i is a %i" (index + 1) die)
        dicelist
        keeps

let loadBoard (record : BoardRecord) =
    board <- record
    game <- { players = game.players; board = board }
    roll <- board.roll        
    displayDie board.dice board.hold
    controlsupdate roll

let loadPlayer (record : PlayerRecord) =
    player <- record
    game <- { players = seq { player; }; board = game.board }
    displayScores player.scores

let toggleMenu _ =
    if closed
    then
        menu.classList.remove "closemenu"
        menu.classList.add "openmenu"
        closed <- false
    else
        menu.classList.remove "openmenu"
        menu.classList.add "closemenu"
        closed <- true

let setMenuContents (title : string) (rows : MenuRowData list) =
    menucontents.innerHTML <- ""
    menutitle.innerText <- title
    List.iter 
        (fun (row : MenuRowData) 
            ->
                let output : Browser.Types.HTMLButtonElement = 
                    unbox window.document.createElement "button"
                output.innerText <- row.Title
                output.onclick <- row.Click
                output.className <- "menurow"
                menucontents.appendChild output |> ignore
        )
        rows


let setMenuHighScore (load : PlayerRecord) =
    menucontents.innerHTML <- ""
    menutitle.innerText 
        <- (sprintf "High Score: %i" (Seq.sumBy (fun (x : int) -> if x = -1 then 0 else x) load.scores))

    let container : Browser.Types.HTMLDivElement =
        unbox window.document.createElement "div"
    container.className <- "table"

    let upper : Browser.Types.HTMLDivElement = 
        unbox window.document.createElement "div"
    upper.className <- "upper-table colgroup highscore"

    let lower : Browser.Types.HTMLDivElement = 
        unbox window.document.createElement "div"
    lower.className <- "lower-table colgroup highscore"

    let header : Browser.Types.HTMLDivElement =
        unbox window.document.createElement "div"
    header.className <- "header"
    let headertitle : Browser.Types.HTMLSpanElement =
        unbox window.document.createElement "span"
    headertitle.className <- "diceroll"
    headertitle.innerText <- "Dice:"
    let headermax : Browser.Types.HTMLSpanElement =
        unbox window.document.createElement "span"
    headermax.innerText <- "Max:"
    let headervalue : Browser.Types.HTMLSpanElement =
        unbox window.document.createElement "span"
    headervalue.className <- "scoreheader"
    headervalue.innerText <- "Score:"
    header.appendChild headertitle |> ignore
    header.appendChild headermax |> ignore
    header.appendChild headervalue |> ignore
    let displayrows = 
        Seq.mapi
            (fun index inval ->
                let row : Browser.Types.HTMLDivElement =
                    unbox window.document.createElement "div"
                let rowlabel : Browser.Types.HTMLSpanElement =
                    unbox window.document.createElement "span"
                let rowmax : Browser.Types.HTMLSpanElement =
                    unbox window.document.createElement "span"
                let rowvalue : Browser.Types.HTMLSpanElement =
                    unbox window.document.createElement "span"
                rowvalue.innerText 
                    <- (sprintf "%i" (if inval = -1 then 0 else inval))
                row.className <- "row"
                rowvalue.className <- "output"
                rowlabel.innerText 
                    <- ((List.item index rows).Title.innerText)
                rowmax.innerText 
                    <- ((List.item index rows).Label.childNodes.[3] 
                        :?> Browser.Types.HTMLSpanElement).innerText
                row.appendChild rowlabel |> ignore
                row.appendChild rowmax |> ignore
                row.appendChild rowvalue |> ignore
                row)
            load.scores
        |> Seq.toArray
    upper.appendChild header            |> ignore
    upper.appendChild displayrows.[0]   |> ignore
    upper.appendChild displayrows.[1]   |> ignore
    upper.appendChild displayrows.[2]   |> ignore
    upper.appendChild displayrows.[3]   |> ignore
    upper.appendChild displayrows.[4]   |> ignore
    upper.appendChild displayrows.[5]   |> ignore
    upper.appendChild displayrows.[14]  |> ignore
    lower.appendChild displayrows.[6]   |> ignore
    lower.appendChild displayrows.[7]   |> ignore
    lower.appendChild displayrows.[8]   |> ignore
    lower.appendChild displayrows.[9]   |> ignore
    lower.appendChild displayrows.[10]  |> ignore
    lower.appendChild displayrows.[11]  |> ignore
    lower.appendChild displayrows.[12]  |> ignore
    lower.appendChild displayrows.[13]  |> ignore
    container.appendChild upper         |> ignore
    container.appendChild lower         |> ignore
    menucontents.appendChild container  |> ignore
    
