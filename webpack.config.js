// Note this only includes basic configuration for development mode.
// For a more comprehensive configuration check:
// https://github.com/fable-compiler/webpack-config-template

var path = require("path");

module.exports = 
[
    {
        name: "dev",
        mode: "development",
        entry: "./src/App.fs.js",
        output: {
            path: path.join(__dirname, "./yachtuwp/js"),
            filename: "bundle.js",
        },
        devServer: {
            publicPath: "/js/",
            contentBase: "./yachtuwp",
            port: 8080,
        },
        module: { }
    },
    {
        name: "prod",
        mode: "production",
        entry: "./src/App.fs.js",
        output: {
            path: path.join(__dirname, "./yachtuwp/js"),
            filename: "bundle.js",
        },
        devServer: {
            publicPath: "/js/",
            contentBase: "./yachtuwp",
            port: 8080,
        },
        module: { }
    },
    {
        name: "babelout",
        mode: "production",
        entry: "./src/App.fs.js",
        output: {
            path: path.join(__dirname, "./yachtuwp/js"),
            filename: "legacy.js",
        },
        module: { 
            rules: [{
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            }]
        }
    }
]